﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountWebApi.Exceptions;
using AccountWebApi.Models;
using AccountWebApi.Models.RequestModels;
using Microsoft.EntityFrameworkCore;
using AccountWebApi.Mappers;

namespace AccountWebApi.Services
{
    public class ContactService : IContactService
    {
        private readonly DataContext _context;
        private readonly IContactMapper _contactMapper;

        public ContactService(DataContext context, IContactMapper contactMapper)
        {
            _context = context;
            _contactMapper = contactMapper;
        }

        public async Task<Contact> AddContact(ContactRequest contact)
        {
            var dbContact = new Contact();
            _contactMapper.Map(dbContact, contact);
            _context.Contacts.Add(dbContact);
            await _context.SaveChangesAsync();
            return dbContact;
        }

        public async Task<Contact> GetContact(int id)
        {
            return await _context.Contacts.SingleOrDefaultAsync(m => m.ContactId == id);
        }

        public async Task UpdateContact(int id, ContactRequest contact)
        {
            if (!ContactExists(id))
            {
                throw new ContactNotFoundException();
            }

            var dbContact = new Contact();
            _contactMapper.Map(dbContact, contact);

            _context.Entry(dbContact).State = EntityState.Modified;            

            await _context.SaveChangesAsync();
        }

        private bool ContactExists(int id)
        {
            return _context.Contacts.Any(e => e.ContactId == id);
        }
    }
}
