﻿using AccountWebApi.Models;
using AccountWebApi.Models.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountWebApi.Services
{
    public interface IAccountService
    {
        Task<IEnumerable<Account>> GetAccountsAsync();
        Task<Account> GetAccountAsync(int id);
        Task UpdateAccountAsync(int id, AccountRequest account);
        Task<Account> AddAccountAsync(AccountRequest account);
        Task<Account> AddContactAsync(Account dbAccount, ContactRequest contact);
        Task AddExistingContactAsync(int accountid, int contactId);
    }
}
