﻿using AccountWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AccountWebApi.Exceptions;
using AccountWebApi.Models.RequestModels;
using AccountWebApi.Mappers;

namespace AccountWebApi.Services
{
    public class AccountService : IAccountService
    {
        private DataContext _context;
        private readonly IAccountMapper _accountMapper;
        private readonly IContactService _contactService;

        public AccountService(DataContext context, IAccountMapper mapper, IContactService contactService)
        {
            _context = context;
            _accountMapper = mapper;
            _contactService = contactService;
        }

        public async Task<IEnumerable<Account>> GetAccountsAsync()
        {
            return await _context.Accounts.ToListAsync();
        }

        public async Task<Account> GetAccountAsync(int id)
        {
            return await _context.Accounts.Where(m => m.AccountId == id).Include(a => a.Contacts).FirstOrDefaultAsync();
        }

        public async Task UpdateAccountAsync(int id, AccountRequest account)
        {
            if (!AccountExists(id))
            {
                throw new AccountNotFoundException();
            }

            var dbAccount = _context.Accounts.FirstOrDefault(a => a.AccountId == id);
            _accountMapper.Map(dbAccount, account);

            _context.Entry(dbAccount).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }

        private bool AccountExists(int id)
        {
            return _context.Accounts.Any(e => e.AccountId == id);
        }

        public async Task<Account> AddAccountAsync(AccountRequest account)
        {
            var dbAccount = new Account();
            _accountMapper.Map(dbAccount, account);
            _context.Accounts.Add(dbAccount);
            await _context.SaveChangesAsync();
            return dbAccount;
        }

        public async Task<Account> AddContactAsync(Account dbAccount, ContactRequest contact)
        {
            var dbContact = await _contactService.AddContact(contact);
            if(dbAccount.Contacts == null)
            {
                dbAccount.Contacts = new List<Contact>();
            }

            dbAccount.Contacts.Add(dbContact);
            _context.Entry(dbAccount).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return dbAccount;
        }

        public async Task AddExistingContactAsync(int accountid, int contactId)
        {
            var dbAccount = await GetAccountAsync(accountid);
            if(dbAccount == null)
            {
                throw new AccountNotFoundException();
            }

            var dbContact = await _contactService.GetContact(contactId);

            if (dbAccount.Contacts == null)
            {
                dbAccount.Contacts = new List<Contact>();
            }

            dbAccount.Contacts.Add(dbContact);
            _context.Entry(dbAccount).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
