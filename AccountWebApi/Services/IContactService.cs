﻿using AccountWebApi.Models;
using AccountWebApi.Models.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountWebApi.Services
{
    public interface IContactService
    {
        Task<Contact> GetContact(int id);
        Task UpdateContact(int id, ContactRequest contact);
        Task<Contact> AddContact(ContactRequest contact);
    }
}
