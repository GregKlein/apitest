﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AccountWebApi.Models;
using AccountWebApi.Services;
using AccountWebApi.Exceptions;
using AccountWebApi.Models.RequestModels;

namespace AccountWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        private readonly DataContext _context;
        private readonly IAccountService _accountService;

        public AccountController(DataContext context, IAccountService accountService)
        {
            _context = context;
            _accountService = accountService;
        }

        // GET: api/Accounts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAccount([FromRoute] int id)
        {
            var account = await _accountService.GetAccountAsync(id);

            if (account == null)
            {
                return NotFound();
            }

            return Ok(account);
        }

        // PUT: api/Accounts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccount([FromRoute] int id, [FromBody] AccountRequest account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                await _accountService.UpdateAccountAsync(id, account);
            }
            catch (AccountNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/Accounts
        [HttpPost]
        public async Task<IActionResult> PostAccount([FromBody] AccountRequest account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dbAccount = await _accountService.AddAccountAsync(account);

            return CreatedAtAction("GetAccount", new { id = dbAccount.AccountId }, dbAccount);
        }

        [HttpGet]
        [Route("[Controller]/{AccountId}/Contacts")]
        public async Task<IActionResult> GetAllContacts([FromRoute] int AccountId)
        {
            var account = await _accountService.GetAccountAsync(AccountId);

            if (account == null)
            {
                return NotFound();
            }

            return Ok(new { contacts = account.Contacts });
        }

        [HttpPost]
        [Route("[Controller]/{AccountId}/Contacts")]
        public async Task<IActionResult> AddContact([FromRoute] int AccountId, [FromBody] ContactRequest contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dbAccount = await _accountService.GetAccountAsync(AccountId);

            if (dbAccount == null)
            {
                return NotFound();
            }

            dbAccount = await _accountService.AddContactAsync(dbAccount, contact);

            return Ok(dbAccount);
        }

        [HttpPost]
        [Route("[Controller]/{AccountId}/AddExistingContact/{ContactId}")]
        public async Task<IActionResult> AddExistingContact([FromRoute] int AccountId, [FromRoute] int ContactId)
        {
            try
            {
                await _accountService.AddExistingContactAsync(AccountId, ContactId);
            }
            catch (AccountNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}