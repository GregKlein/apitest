﻿using AccountWebApi.Models;
using AccountWebApi.Models.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountWebApi.Mappers
{
    public interface IAccountMapper
    {
        void Map(Account account, AccountRequest accountRequest);
    }
}
