﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountWebApi.Models;
using AccountWebApi.Models.RequestModels;

namespace AccountWebApi.Mappers
{
    public class ContactMapper : IContactMapper
    {
        public void Map(Contact contact, ContactRequest contactRequest)
        {
            contact.AddressLineOne = contactRequest.AddressLineOne;
            contact.AddressLineTwo = contactRequest.AddressLineTwo;
            contact.City = contactRequest.City;
            contact.Country = contactRequest.Country;
            contact.EmailAddress = contactRequest.EmailAddress;
            contact.Name = contactRequest.Name;
            contact.PostalCode = contactRequest.PostalCode;
            contact.State = contactRequest.State;
        }
    }
}
