﻿using AccountWebApi.Models;
using AccountWebApi.Models.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountWebApi.Mappers
{
    public class AccountMapper : IAccountMapper
    {
        public void Map(Account dbAccount, AccountRequest account)
        {
            dbAccount.AddressLineOne = account.AddressLineOne;
            dbAccount.AddressLineTwo = account.AddressLineTwo;
            dbAccount.City = account.City;
            dbAccount.CompanyName = account.CompanyName;
            dbAccount.Country = account.Country;
            dbAccount.PostalCode = account.PostalCode;
            dbAccount.State = account.State;
        }
    }
}
