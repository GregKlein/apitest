# AccountWebApi
This is a simple web API to be able to Access / Create / Modify accounts and contacts.

The API has been deployed to AWS and can be accessed here:  
** http://ec2-18-206-147-29.compute-1.amazonaws.com:8080/ **

## Where are the docs?
The project uses Swagger as a means of self documentation.

_What the hell does that mean?_  
Swagger is API middleware that automatically generates HTML documentation. 
If you open the API up in your web browser [(API link)](http://ec2-18-206-147-29.compute-1.amazonaws.com:8080/), the root page will display an HTML document that documents each controller and each endpoint.  
The document has an interactive GUI that lets you POST / GET / PUT directly from your browser without needing a third party app like Postman or Advanced  Rest Client.
It also shows you the shape of the expected JSON or route params for each request, making it super easy to see how to use the API.

## How to run the API locally  
1.) Clone the project  
2.) Install the .net core 2.0 SDK (download here: https://www.microsoft.com/net/download/macos/build)  
3.) Open your terminal / cmd prompt  
4.) In the terminal, navigate to the ./AccountWebApi folder of the project  
5.) In the terminal run `dotnet run`  
6.) That's it!  

(Please note, I was not able to test this on mac OS.  If you have any trouble let me know!!)

## Technical overview  
The project is broken into the usual Model / Controller scheme.   
Database and Web Request models can be found in the `Models` directory.  
Controllers are in the `Controllers` directory.  There is an `Accounts` controller and a `Contacts` controller, each with the specified `GET`, `PUT`, and `POST` endpoints.    
The `Accounts` controller has a few additional endpoints for dealing with relating `Contacts` with `Accounts` (such as adding a new Contact or an existing Contact to an Account).  
The project also implements a Service Layer (found in the `Services` directory) as a layer of abstraction between the Controller and the Data Access layers.  There is an Account Service and a Contact Service in this folder.  
The project is also set up to use IoC / DI to allow further decoupling.  You can find the IoC registration in `Startup.cs`.  
Finally, unit tests are separated into the `Tests` project.  I only implemented a couple tests, but I added a few unimplmented tests to show some ideas of things that would make for good test cases.  

## Further considerations  
The project uses a simple SQLite database for portability, but if this were a customer facing application a more robust dedicated SQL server would be used.  
It would also make sense to look into some containerization for deployments (I'm a big fan of Docker).  
I've also mostly left security open on this API.  In a real-life deployed API there would need to be some level of security, such as token based authentication (JWT), and CORs settings to allow sites to access the API.

If you have any questions / concerns, comments, please feel free to reach out!  Thanks!
