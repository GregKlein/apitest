﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AccountWebApi.Models;
using AccountWebApi.Models.RequestModels;
using AccountWebApi.Services;

namespace Tests.Mocks
{
    class TestAccountService : IAccountService
    {
        public Task<Account> AddAccountAsync(AccountRequest account)
        {
            var dbAccount = new Account();
            return Task.FromResult(dbAccount);
        }

        public Task<Account> AddContactAsync(Account dbAccount, ContactRequest contact)
        {
            dbAccount = new Account();
            dbAccount.Contacts = new List<Contact>();

            dbAccount.Contacts.Add(new Contact());

            return Task.FromResult(dbAccount);
        }

        public Task AddExistingContactAsync(int accountid, int contactId)
        {
            throw new NotImplementedException();
        }

        public Task<Account> GetAccountAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Account>> GetAccountsAsync()
        {
            throw new NotImplementedException();
        }

        public Task UpdateAccountAsync(int id, AccountRequest account)
        {
            throw new NotImplementedException();
        }
    }
}
