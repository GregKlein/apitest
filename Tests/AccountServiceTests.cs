using AccountWebApi.Models;
using AccountWebApi.Models.RequestModels;
using AccountWebApi.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tests.Mocks;

namespace Tests
{
    [TestClass]
    public class AccountServiceTests
    {
        [TestMethod]
        public void AccountService_AddNewAccount_ReturnsValidAccount()
        {
            var accountService = new TestAccountService();
            var account = accountService.AddAccountAsync(new AccountRequest()).Result;

            Assert.IsTrue(account != null);
        }

        [TestMethod]
        public void AccountService_AddContactToAccount_ReturnsAccountWithContacts()
        {
            var accountService = new TestAccountService();
            var account = accountService.AddContactAsync(new Account(), new ContactRequest()).Result;

            Assert.IsTrue(account.Contacts != null && account.Contacts.Count > 0);
        }

        [TestMethod]
        public void AccountService_GetAccountThatDoesntExist_ReturnsNull()
        {
            Assert.Inconclusive("TODO: implement this test");
        }

        [TestMethod]
        public void AccountService_AddContactToAccountThatDoesntExist_ThrowAccountNotFoundException()
        {
            Assert.Inconclusive("TODO: implement this test");
        }

        [TestMethod]
        public void AccountService_UpdateExistingAccount_ReflectsChanges()
        {
            Assert.Inconclusive("TODO: implement this test");
        }

        [TestMethod]
        public void AccountService_UpdateAccountThatDoesntExist_ThrowsAccountNotFoundException()
        {
            Assert.Inconclusive("TODO: implement this test");
        }
    }
}
